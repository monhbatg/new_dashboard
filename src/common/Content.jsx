import { Switch, Route } from "react-router-dom";
import {
  Content4,
  Content2,
  Content3,
  Content1,
  NotFound,
} from "@/dashboard/components/";

function Content() {
  return (
    <div className="content_container">
      <Switch>
        <Route path="/content1" component={Content1} />
        <Route path="/content2" component={Content2} />
        <Route path="/content3" component={Content3} />
        <Route path="/content4" component={Content4} />
        <Route path="/" component={NotFound} />
      </Switch>
    </div>
  );
}

export default Content;
