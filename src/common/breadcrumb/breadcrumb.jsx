import { Breadcrumb } from "antd";
import { HomeOutlined, UserOutlined } from "@ant-design/icons";

export default function MainBreadcrumb({ text1, text2 }) {
  return (
    <div className="breadcrumb">
      <Breadcrumb>
        <Breadcrumb.Item href="">
          <HomeOutlined />
        </Breadcrumb.Item>
        <Breadcrumb.Item href="">
          <UserOutlined />
          <span>{text1}</span>
        </Breadcrumb.Item>
        <Breadcrumb.Item>{text2}</Breadcrumb.Item>
      </Breadcrumb>
    </div>
  );
}
