let vars = {
  menus: [
    { key: "/content1", name: "Content 1", link: "/content1" },
    { key: "/content2", name: "Content 2", link: "/content2" },
    { key: "/content3", name: "Content 3", link: "/content3" },
    { key: "/content4", name: "Content 1231", link: "/content4" },
  ],
};
let getVars = (name) => {
  return vars[name];
};
export { vars, getVars };
