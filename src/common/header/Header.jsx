import { Layout } from "antd";
import logo from "../../assets/logo.png";
import { Menu, Dropdown, Button } from "antd";
import { UserOutlined } from "@ant-design/icons";

const HeaderContainer = Layout;

export default function Header({ user }) {
  const menu = (
    <Menu>
      {user.map((q) => {
        return <Menu.Item key={q.key}>{q.value}</Menu.Item>;
      })}

      <Menu.Divider />
      <Menu.Item onClick={() => {}}>Log Out</Menu.Item>
    </Menu>
  );

  return (
    <HeaderContainer>
      <div className="header_container">
        <div className="header_logo_container">
          {<img className="header_logo" src={logo} alt="header_logo" />}
        </div>
        <div className="dropdown">
          <Dropdown
            overlay={menu}
            placement="bottomRight"
            className="header_dropdown_button"
            trigger={["click"]}
          >
            <Button size="large" shape="round" icon={<UserOutlined />}>
              M E
            </Button>
          </Dropdown>
        </div>
      </div>
    </HeaderContainer>
  );
}
