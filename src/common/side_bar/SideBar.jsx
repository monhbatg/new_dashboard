import React from "react";
import { Layout } from "antd";
import SideBarItem from "./SideBarItem";

const { Sider } = Layout;

export default function SideBar(props) {
  return (
    <Sider
      className="sider"
      breakpoint="lg"
      collapsedWidth="0"
      onBreakpoint={(broken) => {
        console.log(broken);
      }}
      onCollapse={(collapsed, type) => {
        console.log(collapsed, type);
      }}
    >
      <div className="sidebar_title">Dashboard</div>
      <SideBarItem className="sidebar_item" {...props} />
    </Sider>
  );
}
