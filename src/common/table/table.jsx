import { Table } from "antd";

export default function MainTable({ dataSource, columns }) {
  return <Table dataSource={dataSource} columns={columns} />;
}
