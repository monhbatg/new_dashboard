import Header from "../common/header/Header";

import { SideBar } from "@/common/side_bar/";
import { Layout } from "antd";
import { Content, getVars } from "@/common/";
import { useState } from "react";

export default function Dashboard({ location }) {
  let [user, setUser] = useState([
    { key: 0, value: " Meettien EEMELI" },
    { key: 1, value: " user@email.com" },
    { key: 2, value: " 35847004004" },
  ]);
  const sidebar_data = getVars("menus");
  return (
    <>
      <Header user={user} />
      <Layout style={{ minHeight: "100vh" }}>
        <SideBar sidebar={sidebar_data} defaultPage={location.pathname} />
        <Layout>
          <Content />
        </Layout>
      </Layout>
    </>
  );
}
