import Content1 from './Content1';
import NotFound from './NotFound';
import Content2 from './Content2';
import Content3 from './Content3';
import Content4 from './Content4';

export { NotFound, Content1, Content2, Content3, Content4 }