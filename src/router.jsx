import { HashRouter as BrowserRouter, Route, Switch } from "react-router-dom";
import Dashboard from "./dashboard/Dashboard";
import Authentication from "./common/authentication/Authentication";
export default function Router() {
  return (
    <>
      <BrowserRouter>
        <Switch>
          <Route path="/login" component={Authentication} />
          <Route path="/" component={Dashboard} />
        </Switch>
      </BrowserRouter>
    </>
  );
}
